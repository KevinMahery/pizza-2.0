"use strict";

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PizzaSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name : {type: String, unique: true, required:true},
    ingredients: {type: [String], required: true},
    description : String,
    price : Number,
    size : String,
    cooking: {type: [String], required: true},
    cookingTeam: {type: [String], required: true}
});

module.exports = mongoose.model('pizza', PizzaSchema);
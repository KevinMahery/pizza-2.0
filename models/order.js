"use strict";

//TODO : création de schema pour les commandes.
const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    pizza: {type: mongoose.Schema.Types.ObjectId, ref : 'Pizza', require: true},
    quantity: {type: Number, default: 1}
});

const mongooseLoader = require('../loaders/mongooseLoader');
const Pizza = require('../models/pizza');
const pizzas = [{
    name : "margarita",
    ingredients : ["fromage", "sauce tomate"],
    description : "La pizza classique, un incontournable",
    price : 8,
    size : "S", "M", "XL",
    preparation: "Pour chaque ingrédient qui compose une pizza de taille S, il faut compter 1 minute de préparation supplémentaire. <br> Il faut prévoir 30 % de temps supplémentaire, pour une pizza de taille M, et encore 30 % de temps supplémentaire pour une taille XL.",
    cooking: "Il faut savoir que le four à pizza permet de cuire 4 pizzas en même temps et que leur cuisson prend 3 minutes quelque soit la taille. Le four n'est utilisée que pour une commande à la fois.",
    cookingTeam: "Il y a deux pizzailo, l'un est chargé et de préparer les pizzas et l'autre s'occupe de la cuisson dès lors que 4 pizzas sont prêtes à être cuites."
},{
    name : "régina",
    ingredients : ["fromage", "sauce tomate", "jambon", "olives"],
    description : "savoureuse et croustillante",
    price : 9.50,
    size : "S", "M", "XL",
    preparation: "Pour chaque ingrédient qui compose une pizza de taille S, il faut compter 1 minute de préparation supplémentaire.  <br>Il faut prévoir 30 % de temps supplémentaire, pour une pizza de taille M, et encore 30 % de temps supplémentaire pour une taille XL.",
    cooking: "Il faut savoir que le four à pizza permet de cuire 4 pizzas en même temps et que leur cuisson prend 3 minutes quelque soit la taille. Le four n'est utilisée que pour une commande à la fois.",
    cookingTeam: "Il y a deux pizzailo, l'un est chargé et de préparer les pizzas et l'autre s'occupe de la cuisson dès lors que 4 pizzas sont prêtes à être cuites."
},{
    name : "chicken",
    ingredients : ["fromage", "sauce tomate", "poulet", "olives"],
    description : "une variante des plus recommandées par nos clients",
    price: 9.50,
    size : "S", "M", "XL",
    preparation: "Pour chaque ingrédient qui compose une pizza de taille S, il faut compter 1 minute de préparation supplémentaire. <br> Il faut prévoir 30 % de temps supplémentaire, pour une pizza de taille M, et encore 30 % de temps supplémentaire pour une taille XL.",
    cooking: "Il faut savoir que le four à pizza permet de cuire 4 pizzas en même temps et que leur cuisson prend 3 minutes quelque soit la taille. Le four n'est utilisée que pour une commande à la fois.",
    cookingTeam: "Il y a deux pizzailo, l'un est chargé et de préparer les pizzas et l'autre s'occupe de la cuisson dès lors que 4 pizzas sont prêtes à être cuites."
}]


async function seedPizzas() {
    try {
        await mongooseLoader();
        for(let pizza of pizzas) {
            await Pizza.findOneAndUpdate({name : pizza.name}, pizza, {new:true, upsert :true, setDefaultsOnInsert: true, runValidators:true});
        }
        console.log('Pizzas added to database');

    } catch(err) {
        console.log(err)
    }
}


module.exports = mongoose.model('Order', orderSchema);
const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const pizzaRoutes = require('./routes/pizza');
const orderRoutes = require('./routes/order');

mongoose.connect('mongodb+srv://kevin_mahery:' + 
    process.env.MONGO_ATLAS_PW +
    '@node-pizzas.d2nqd.mongodb.net/node-pizzas?retryWrites=true&w=majority',
    {
        useMongoClient: true
    });

mongoose.Promise = global.Promise;

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(mongoose('mongoose'));

app.use((req, res, next) =>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

//Routes which should handle requests
app.use('/pizza', pizzaRoutes);
app.use('/order', orderRoutes);

app.use((req, res, next) => {
    const error = new Error('introuvable');
    error.status(404);
    next(error);
})

app.use((error, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = router;
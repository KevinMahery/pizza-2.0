const { post } = require('./app');

var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    port = process.env.PORT || 3000,
    app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

mongoose.connect('mongoose://localhost/pizza');
var pizzaSchema = mongoose.Schema({
    name: String,
    price: String,
    ingredient: String,
    size: String,
    cooking: String,
    cookingTeam: String,
});
var Pizza = mongoose.model('Pizza', pizzaSchema);

var router = express.Router();
router.route('/')
    .get(function(req, res) {
        Pizza.find(function(err, pizza) {
            res.send(pizza);
        });
    });
    post(function(req, res){
        var pizza = new Pizza();
        pizza.name = req.body.name;
        pizza.price = req.body.price;
        pizza.ingredient = req.body.ingredient;
        pizza.size = req.body.size;
        pizza.cooking = req.body.cooking;
        pizza.cookingTeam = req.body.cookingTeam;
        pizza.save(function(err){
            if(err){
                res.send(err);
            }
            res.send({message: 'book created'});
        });
    });

    app.use('./app', router);

    app.listen(port, function() {
        console.log('Listening on port' + port);
    })
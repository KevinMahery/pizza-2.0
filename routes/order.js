"use strict";

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('./models/order');
const Pizza = require('./models/pizza');

/* TODO : routes pour créer et accéder aux commandes */
router.get('/', (req, res, next) => {
    Order.find()
    .select('pizza quantity_id')
    .exec()
    .then(docs => {
        res.status(200).json({
            count: docs.length,
            orders: docs.map(doc => {
                return {
                    _id: doc._id,
                    pizza: doc.pizza,
                    quantity: doc.quantity,
                    request: {
                        type: 'GET',
                        url: 'https://localhost:3000/order/' + doc._id
                    }
                }
            });
           
        });
    })
    .catch(err => {
        res.status(500).json({
            error: err
        });
    });
});

router.post('/', (req, res, next) => {
    Pizza.findById(req.body.pizzaId)
    .then(pizza => {
        if (!pizza) {
            return res.status(404).json({
                message: "Pizza not found"
            });
        }
        const order = new Order ({
        _id: mongoose.Types.ObjectId(),
        pizzaId: req.body.pizzaId,
        quantity: req.body.quantity,
        ingredients: req.body.ingredients,
        description: req.body.description,
        size: req.body.size,
        cooking: req.body.cooking,
        cookingTeam: req.body.cookingTeam        
    });
    return order .save();
   })
    .then(result => {
        console.log(result);
        res.status(201).json({
            message: 'Order stored',
            createOrder: {
                _id: result._id,
                pizza: result.pizza,
                quantity: result.quantity
            },
            request: {
                type: 'GET',
                url: 'https://localhost:3000/order/' + result._id
            }
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });

router.get('/:orderId', (req, res, next) => {
    Order.findById(req.params.orderId)
    .exec()
    .then(order => {
        if  (!order){
            return res.status(404).json({
                message: 'Order not found'
            });
        }
        res.status(200).json({
            order: order,
            request: {
                type: 'GET',
                url: 'https://localhost:3000/orders'
            }
        });
    });
    .catch(err => {
        res.status(500).json({
            error: err
        });
    });
});

router.delete('/:orderId', (req, res, next) => {
    Order.remove({_id: req.params.orderId})
    .exec()
    .then(result => {
        res.status(200).json({
            message: 'Order deleted',
            request: {
                type: 'POST',
                url: 'https://localhost:3000/order',
                body: {pizzaId: 'ID', quantity:'Number'}
            }
        });
    });
    .catch(err => {
        res.status(500).json({
            error: err
        });
    });
});

module.exports = router;
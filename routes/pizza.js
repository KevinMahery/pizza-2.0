"use strict"

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Pizza = require('../models/pizza');

router.get('/', (req, res, next) => {
    Pizza.find()
    .select('name price_id')
    .exec()
    .then(docs => {
        const response = {
            count: docs.length,
            pizza: docs.map(doc => {
                return {
                    name: doc.name,
                    price: doc.price,
                    _id: doc.id,
                    request: {
                    type: 'GET',
                    url: 'https://localhost:3000/pizza/' + doc._id
                    }   
                }
                
            })
        };
        //if (docs.length >=0) {
            res.status(200).json(response);
       // } else {
       //     res.status(404).json({
        //        message: 'No entries found'
        //    });
       // }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
});

router.post('/', (req, res, next) {
        const pizza = new Pizza({
            _id: new mongoose.Types.ObjectId(),
            name: req.body.name,
            price: req.body.price,
            ingredients: req.body.ingredients,
            description: req.body.description,
            size: req.body.size,
            cooking: req.body.cooking,
            cookingTeam: req.body.cookingTeam
        });
        product
            .save()
            .then(result => {
                console.log(result);
                res.status(201).json({
                    message: "Created product successfully",
                    createdPizza: {
                        name: result.name,
                        price: result.price,
                        _id: result._id,
                        request: {
                            type: 'GET',
                            url: 'https://localhost:3000/pizza/' + result._id
                        }
                    }
                })
                    .catch(err => {
                        console.log(err);
                        res.status(500).json({
                            error: err
                        });
                    });
            });

        router.get('/:pizzaId', (req, res, next) => {
            const id = req.params.pizzaId;
            Pizza.findById(id)
                .select('name price_id')
                .exec()
                .then(doc => {
                    console.log("From database, doc");
                    if (doc) {
                        res.status(200).json({
                            product: doc,
                            request: {
                                type: 'GET',
                                url: 'http://localhost:3000/pizza'
                            }
                        });
                    } else {
                        res.status(404).json({ message: 'No valid entry found for provided ID' });
                    }
                })
                .catch(err => console.log(err));
            res.status(500).json({ error: err });
        });

        router.patch('/:pizzaId', (req, res, next) => {
            const id = req.params.pizzaId;
            const updateOps = {};
            for (const ops of req.body) {
                updateOps[ops.propName] = ops.value;
            }
            Pizza.update({ _id: id }, { $set: updateOps })
                .exec()
                .then(result => {
                    console.log(result);
                    res.status(200).json({
                        message: 'Pizza updated',
                        request: {
                            type: 'GET',
                            url: 'https://localhost:3000/pizza/' + id
                        }
                    });
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        error: err
                    });
                });
        });

        router.delete('/:pizzaId', (req, res, next) => {
            const id = req.params.pizzaId;
            Pizza.remove({ _id: id })
                .exec()
                .then(result => {
                    res.status(200).json({
                        message: 'Pizza deleted',
                        request: {
                            type: 'POST',
                            url: 'https://localhost:3000/pizza',
                            body: { name: 'String', price: 'Number' }
                        }
                    });
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        error: err
                    });
                });
        });

        /* fichier pour les routes relatives aux pizzas*/
        router.route('/pizza').post(async (req, res) => {
            try {
                let pizza = new Pizza(req.body);
                pizza = await pizza.save();
                return res.status(200).json(pizza);
            } catch (err) {
                console.log(err);
            }
        });

        router.route('/pizza/:id').get(async (req, res) => {
            try {
                const pizza = await Pizza.findById(req.params.id);
                return res.status(200).json(pizza);
            } catch (err) {
                console.log(err);
            }
        });

        router.route('/pizzas').get(async (req, res) => {
            try {
                const pizzas = await Pizza.find();
                return res.status(200).json(pizzas);
            } catch (err) {
                console.log(err);
            }
        });

        module.exports = router;
    }
"use strict";

const mongooseLoader = require('./mongooseLoader');
const expressLoader = require('./expressLoader');
const routeLoader = require('./routeLoader');

const URI = "mongodb+srv://kevin_mahery:kevin123456@node-pizzas.d2nqd.mongodb.net/node-pizzas?retryWrites=true&w=majority";

const connectDB = async() => {
   await mongoose.connect(URI);
   console.log('db connected');
}

module.exports = async function(app) {
    await mongooseLoader();
    await expressLoader(app);
    await routeLoader(app);
}
module.exports = connectDB;